resource "aws_s3_bucket" "app_public_files-01" {
  bucket        = "${local.prefix}-files-01"
  acl           = "public-read"
  force_destroy = true
}

